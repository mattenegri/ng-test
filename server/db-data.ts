export const COURSES: any = {

  12: {
    id: 12,
    titles: {
      description: 'Le basi del Padel',
      longDescription: 'Introduzione al mondo del padel'
    },
    iconUrl: 'https://www.forumroma.it/wp-content/uploads/2017/06/padel.jpg',
    category: 'BEGINNER',
    seqNo: 0,
    url: 'le-basi-del-padel',
    lessonsCount: 3,
  },

  2: {
    id: 2,
    titles: {
      description: 'Le basi del tennis',
      longDescription: 'Corso introduttivo al mondo del tennis'
    },
    iconUrl: 'https://www.repstatic.it/video/photo/2022/05/28/836111/836111-thumb-full-720-nadal_e_zio.jpg',
    lessonsCount: 3,
    category: 'BEGINNER',
    seqNo: 1,
    url: 'le-basi-del-tennis'
  },

  3: {
    id: 3,
    titles: {
      description: 'Bandeja e Vibora',
      longDescription: 'Introduzione ai due colpi più importanti del padel'
    },
    iconUrl: 'https://www.tuttosulpadel.com/wp-content/uploads/bandeja-impacto-3.jpg',
    courseListIcon: 'https://www.tuttosulpadel.com/wp-content/uploads/bandeja-impacto-3.jpg',
    category: 'BEGINNER',
    lessonsCount: 10,
    seqNo: 2,
    url: 'bandeja-vibora'
  },

  4: {
    id: 4,
    titles: {
      description: 'Il diritto e il rovescio',
      longDescription: 'Introduzioni ai colpi fondamentali del tennis'
    },
    iconUrl: 'https://www.repstatic.it/video/photo/2022/05/28/836111/836111-thumb-full-720-nadal_e_zio.jpg',
    courseListIcon: 'https://www.repstatic.it/video/photo/2022/05/28/836111/836111-thumb-full-720-nadal_e_zio.jpg',
    category: 'BEGINNER',
    lessonsCount: 5,
    seqNo: 3,
    url: 'diritto-rovescio'
  },

  1: {
    id: 1,
    titles: {
      description: 'Corso completo Padel',
      longDescription: '10 lezioni nel mondo del padel'
    },
    iconUrl: 'https://www.forumroma.it/wp-content/uploads/2017/06/padel.jpg',
    lessonsCount: 10,
    category: 'BEGINNER',
    seqNo: 4,
    url: 'corso-completo-padel'
  },

  5: {
    id: 5,
    titles: {
      description: 'Corso completo tennis',
      longDescription: '10 lezioni sul mondo del tennis'
    },
    iconUrl: 'https://www.repstatic.it/video/photo/2022/05/28/836111/836111-thumb-full-720-nadal_e_zio.jpg',
    courseListIcon: 'https://www.repstatic.it/video/photo/2022/05/28/836111/836111-thumb-full-720-nadal_e_zio.jpg',
    category: 'BEGINNER',
    lessonsCount: 10,
    seqNo: 5,
    url: 'corso-completo-tennis'
  },

  6: {
    id: 6,
    titles: {
      description: 'Fondamenti di tattica del padel',
      longDescription: 'Corso professionalizzante sulla tattica del padel'
    },
    iconUrl: 'https://www.tuttosulpadel.com/wp-content/uploads/bandeja-impacto-3.jpg',
    courseListIcon: 'https://www.tuttosulpadel.com/wp-content/uploads/bandeja-impacto-3.jpg',
    category: 'ADVANCED',
    lessonsCount: 11,
    seqNo: 6,
    url: 'tattica-nel-padel'
  },

  7: {
    id: 7,
    titles: {
      description: 'Fondamenti di tattica nel tennis',
      longDescription: 'Percorso avanzanto nella tattica del tennis'
    },
    iconUrl: 'https://www.repstatic.it/video/photo/2022/05/28/836111/836111-thumb-full-720-nadal_e_zio.jpg',
    courseListIcon: 'https://www.repstatic.it/video/photo/2022/05/28/836111/836111-thumb-full-720-nadal_e_zio.jpg',
    category: 'ADVANCED',
    lessonsCount: 8,
    seqNo: 7,
    url: 'tattica-nel-tennis'
  },

  8: {
    id: 8,
    titles: {
      description: 'A tutto tennis',
      longDescription: 'La preparazione fisica nel tennis'
    },
    iconUrl:'https://www.repstatic.it/video/photo/2022/05/28/836111/836111-thumb-full-720-nadal_e_zio.jpg',
    courseListIcon: 'https://www.repstatic.it/video/photo/2022/05/28/836111/836111-thumb-full-720-nadal_e_zio.jpg',
    category: 'ADVANCED',
    seqNo: 8,
    url: 'preparazione-atletica-tennis'
  },

  9: {
    id: 9,
    titles: {
      description: 'Tennis Course',
      longDescription: 'Corso completo di tennis'
    },
    iconUrl: 'https://www.repstatic.it/video/photo/2022/05/28/836111/836111-thumb-full-720-nadal_e_zio.jpg',
    courseListIcon: 'https://www.repstatic.it/video/photo/2022/05/28/836111/836111-thumb-full-720-nadal_e_zio.jpg',
    category: 'BEGINNER',
    seqNo: 9,
    url: 'tennis-course'
  },

  10: {
    id: 10,
    titles: {
      description: 'Introduzione al padel',
      longDescription: 'Learn the core RxJs Observable Pattern as well and many other Design Patterns for building Reactive Angular Applications.'
    },
    iconUrl: 'https://www.tuttosulpadel.com/wp-content/uploads/bandeja-impacto-3.jpg',
    courseListIcon: 'https://www.tuttosulpadel.com/wp-content/uploads/bandeja-impacto-3.jpg',
    category: 'BEGINNER',
    seqNo: 10,
    url: 'introduzione-padel'
  },

  11: {
    id: 11,
    titles: {
      description: 'Esercizi base padel',
      longDescription: 'Build Applications with the official Angular Widget Library'
    },
    iconUrl: 'https://www.tuttosulpadel.com/wp-content/uploads/bandeja-impacto-3.jpg',
    category: 'BEGINNER',
    seqNo: 11,
    url: 'esercizi-base-padel'
  }

};


export const LESSONS = {

  1: {
    id: 1,
    'description': 'Introduzione',
    'duration': '4:17',
    'seqNo': 1,
    courseId: 5
  },
  2: {
    id: 2,
    'description': 'Le regole',
    'duration': '2:07',
    'seqNo': 2,
    courseId: 5
  },
  3: {
    id: 3,
    'description': 'Attrezzi di gioco',
    'duration': '2:33',
    'seqNo': 3,
    courseId: 5
  },
  4: {
    id: 4,
    'description': 'Il diritto',
    'duration': '4:44',
    'seqNo': 4,
    courseId: 5
  },
  5: {
    id: 5,
    'description': 'Il rovescio',
    'duration': '2:55',
    'seqNo': 5,
    courseId: 5
  },
  6: {
    id: 6,
    'description': 'La volee di diritto',
    'duration': '3:27',
    'seqNo': 6,
    courseId: 5
  },
  7: {
    id: 7,
    'description': 'La volee di rovescio',
    'duration': '9:22',
    'seqNo': 7,
    courseId: 5
  },
  8: {
    id: 8,
    'description': 'Lo smash',
    'duration': '1:26',
    'seqNo': 8,
    courseId: 5
  },
  9: {
    id: 9,
    'description': 'Il servizio',
    'duration': '2:08',
    'seqNo': 9,
    courseId: 5
  },
  10: {
    id: 10,
    'description': 'Tattica di base',
    'duration': '4:01',
    'seqNo': 10,
    courseId: 5
  },


  // Padel Course
  11: {
    id: 11,
    'description': 'La tattica',
    'duration': '08:19',
    'seqNo': 1,
    courseId: 6
  },

  12: {
    id: 12,
    'description': 'Bandeja e Smash',
    'duration': '04:17',
    'seqNo': 2,
    courseId: 6
  },

  13: {
    id: 13,
    'description': 'La chiquita e attacco',
    'duration': '06:05',
    'seqNo': 3,
    courseId: 6
  },

  14: {
    id: 14,
    'description': 'Prendere la rete',
    'duration': '03:57',
    'seqNo': 4,
    courseId: 6
  },

  15: {
    id: 15,
    'description': 'Il pallonetto',
    'duration': '06:00',
    'seqNo': 5,
    courseId: 6
  },
  16: {
    id: 16,
    'description': 'Quando arretrare',
    'duration': '04:53',
    'seqNo': 6,
    courseId: 6
  },
  17: {
    id: 17,
    'description': 'Controparete o pallonetto?',
    'duration': '09:14',
    'seqNo': 7,
    courseId: 6
  },
  18: {
    id: 18,
    'description': 'La posizione corretta',
    'duration': '06:08',
    'seqNo': 8,
    courseId: 6
  },
  19: {
    id: 19,
    'description': 'Quando dobbiamo difendere',
    'duration': '08:50',
    'seqNo': 9,
    courseId: 6
  },
  20: {
    id: 20,
    'description': 'Attacco in controtempo',
    'duration': '05:46',
    'seqNo': 10,
    courseId: 6
  },
  21: {
    id: 21,
    'description': 'I punti di equilibrio',
    'duration': '06:31',
    'seqNo': 11,
    courseId: 6
  },


  // Tennis course

  22: {
    id: 22,
    'description': 'Introduzione alla tattica',
    'duration': '07:19',
    'seqNo': 1,
    courseId: 7
  },
  23: {
    id: 23,
    'description': 'Il punto di equilibrio',
    'duration': '6:59',
    'seqNo': 2,
    courseId: 7
  },
  24: {
    id: 24,
    'description': 'Tattiche di risposta',
    'duration': '7:28',
    'seqNo': 3,
    courseId: 7
  },
  25: {
    id: 25,
    'description': 'Serve and vollee',
    'duration': '10:17',
    'seqNo': 4,
    courseId: 7
  },

  26: {
    id: 26,
    'description': 'La manovra',
    'duration': '09:50',
    'seqNo': 5,
    courseId: 7
  },
  27: {
    id: 27,
    'description': 'Gestire la partita',
    'duration': '04:44',
    'seqNo': 6,
    courseId: 7
  },
  28: {
    id: 28,
    'description': 'I punti fondamentali, la gestione',
    'duration': '06:07',
    'seqNo': 7,
    courseId: 7
  },
  29: {
    id: 29,
    'description': 'Rovescio lungolinea',
    'duration': '5:38',
    'seqNo': 8,
    courseId: 7
  },

  // tennis

  30: {
    id: 30,
    description: 'Le regole',
    'duration': '5:38',
    'seqNo': 1,
    courseId: 1
  },

  31: {
    id: 31,
    description: 'Obiettivi',
    'duration': '5:12',
    'seqNo': 2,
    courseId: 1
  },

  32: {
    id: 32,
    description: 'Attrezzi di gioco',
    'duration': '4:07',
    'seqNo': 3,
    courseId: 1
  },

  33: {
    id: 33,
    description: 'Il diritto',
    'duration': '7:32',
    'seqNo': 4,
    courseId: 1
  },

  34: {
    id: 34,
    description: 'Il rovescio',
    'duration': '6:28',
    'seqNo': 5,
    courseId: 1
  },

  35: {
    id: 35,
    description: 'La bandeja',
    'duration': '4:38',
    'seqNo': 6,
    courseId: 1
  },

  36: {
    id: 36,
    description: 'La vibora',
    'duration': '7:54',
    'seqNo': 7,
    courseId: 1
  },

  37: {
    id: 37,
    description: 'Lo smash',
    'duration': '5:31',
    'seqNo': 8,
    courseId: 1
  },

  38: {
    id: 38,
    description: 'Utilizzo della parete',
    'duration': '8:19',
    'seqNo': 9,
    courseId: 1
  },

  39: {
    id: 39,
    description: 'Volee',
    'duration': '7:05',
    'seqNo': 10,
    courseId: 1
  },


  // Angular Testing Course

  40: {
    id: 40,
    description: 'Come si gioca',
    'duration': '5:38',
    'seqNo': 1,
    courseId: 12
  },

  41: {
    id: 41,
    description: 'Le regole del padel',
    'duration': '5:12',
    'seqNo': 2,
    courseId: 12
  },

  42: {
    id: 42,
    description: 'Dritto e Rovescio',
    'duration': '4:07',
    'seqNo': 3,
    courseId: 12
  }


};




export function findCourseById(courseId: number) {
  return COURSES[courseId];
}

export function findLessonsForCourse(courseId: number) {
  return Object.values(LESSONS).filter(lesson => lesson.courseId == courseId);
}
